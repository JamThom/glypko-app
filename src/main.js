import Vue from 'vue'
import VueRouter from 'vue-router'
import VueRangeSlider from 'vue-range-slider'
import App from './App.vue'
import * as components from './components'
import routerConfig from './router-config'
import Vuex from 'vuex'
import VueCarousel from 'vue-carousel'
import store from 'Store'
import VueYouTubeEmbed from 'vue-youtube-embed'

for (let d in components) { Vue.component(components[d].name, components[d]) }
Vue.use(VueRouter)
Vue.use(VueCarousel)
Vue.use(VueRangeSlider)
Vue.use(VueYouTubeEmbed)

new Vue({
  el: '#app',
  store,
  render: h => h(App),
  router: new VueRouter(routerConfig)
})
