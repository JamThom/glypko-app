import anime from 'animejs'

export default {
  mounted () {
    anime.timeline()
      .add({
        targets: this.$el.children,
        opacity: [0,1],
        easing: 'easeInOutQuad',
        duration: 500
      })
  },
  methods: {
    go (to) {
      anime.timeline()
        .add({
          targets: this.$el.children,
          opacity: [1,0],
          easing: 'easeInOutQuad',
          duration: 500,
          complete: () => to === -1 ? this.$router.back() : this.$router.push(to)
        })
    }
  }
}
