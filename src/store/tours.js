import { OPEN_COMING_SOON_POPUP, CLOSE_POPUP } from './constants'

import img1 from 'Photos/tour1.jpg'
import img2 from 'Photos/tour2.jpg'
import img3 from 'Photos/tour3.jpg'
import img4 from 'Photos/tour4.jpg'

export default {
  state: {
    list: [{
      name: 'Tour through Brandts',
      image: img1,
      description: 'Explore the development and significance of Danish art.'
    },{
      name: 'Theas Treasurehunt',
      image: img2,
      description: 'Join Thea on her quest through the collection, as she learns about art to paint her own picture.'
    },{
      name: 'people and relations',
      image: img3,
      description: 'How have people - alone and socially been depicted through different art periods?'
    },{
      name: 'Colours',
      image: img4,
      description: 'Interact with the collection, and find out how colour influence the way we perceive art.'
    }]
  }
}
