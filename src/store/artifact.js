import { SET_ARTIFACT_ID } from './constants'

import koyer from 'Assets/photos/koyer.jpg'
import koyer1 from 'Assets/photos/koyer11.jpg'
import koyer1a from 'Assets/photos/koyer1a.png'
import koyer1b from 'Assets/photos/koyer1b.png'
import koyer1c from 'Assets/photos/koyer1c.png'
import koyer2 from 'Assets/photos/koyer2.png'
import koyer3 from 'Assets/photos/koyer3.png'
import koyer4 from 'Assets/photos/koyer4.png'
import koyer2a from 'Assets/photos/koyer2a.png'
import koyer2b from 'Assets/photos/nordicart_2.jpg'
import koyer2c from 'Assets/photos/mod.jpg'

import richard1 from 'Assets/photos/richard1.png'
import richard2 from 'Assets/photos/richard2.png'
import richard3a from 'Assets/photos/rich.jpg'
import richard3b from 'Assets/photos/wee.jpg'
import richard3 from 'Assets/photos/richard3.png'
import richard4 from 'Assets/photos/richard4.png'

import vilhel from 'Assets/photos/vilhel.jpg'
import vilhelm1 from 'Assets/photos/vilhelm1.png'
import vilhelm2 from 'Assets/photos/vilhelm2.png'
import vilhelm3b from 'Assets/photos/roman2.png'
import vilhelm3c from 'Assets/photos/rude.png'
import vilhelm3a from 'Assets/photos/lundstrom4.png'
import vilhelm4 from 'Assets/photos/vilhelm4.png'
import vilhelm5 from 'Assets/photos/lund.png'
import vilhelm6a from  'Assets/photos/img22.png'
import vilhelm6b from  'Assets/photos/vilhelm42.png'

export default {
  state: {
    id: 0,
    list: [




      {
      headline: 'Sagone',
      subheadline: 'Richard Mortensen, 1960',
      defaultImage: richard1,
      color: '#ea6f5d',
      questions: [{
        question: 'What does this depict?',
        image: {
          src: richard2,
          caption: 'Sagone, Corsica, France'
        },
        answer: 'Sagone is a coast on the island of Corsica in France. The coast is typical mediterranean, with vegetation, the sea, and small villages.\n\nThe artist - Mortensen, had a close connection to France where he spent many years. Mortensen is an abstract artist, who worked with painting and graphic representation.'
      }, {
        question: 'How does abstract art work?',
        image: [{
          src: richard3,
          caption: 'Richard Mortensen - Sagone 2, 1971'
        }, {
          src: richard3a,
          caption: 'Suite Ribe IV - Lakolk, 1973'
        }, {
          src: richard3b,
          caption: 'Richard Mortensen - Ouest 2 (1956)'
        }],
        answer: 'Abstract art uses a visual language of shape, form, color and line to create a composition which may exist with a degree of independence from visual references in the world. It is as such, an abstract form of art.',
        link: {
          to: '/video/96hl5J47c3k',
          text: 'Watch class'
        }
      }, {
        question: 'Do the colors have any significance?',
        image: richard4,
        answer: 'Color theory is both the science and art of color. It explains how humans perceive color; how colors mix, match or clash; the subliminal (and often cultural) messages colors communicate; and the methods used to replicate color.',
        link: {
          to: '/color',
          text: 'alter colors'
        }
      }]
    },
    
      {
      headline: 'Opstilling med Kander',
      subheadline: 'Vilhelm Lundstrøm, 1930-32',
      defaultImage: vilhelm1,
      color: '#3c66b3',
      questions: [{
        question: 'Who is Vilhelm Lundstrøm?',
        image: {
          src: vilhelm5,
          caption: 'Vilhelm Lundstrøm in front of Pakkassebillede'
        },
        answer: 'Lundstrøm (1993-1950) was one of the most successful Danish modernist painters. He introduced French cubism to Denmark, where his expressionist and flamboyant style was initially frowned upon. He later developed his style of simple compositions consisting of jugs and fruits.',
        link: {
          to: '/bio/lundstrom',
          text: 'full bio'
        }
      }, {
        question: 'What is a still life?',
        image: [{
          src: vilhelm3a,
          caption: 'Vilhelm Lundstrøm - Opstilling'
        }, {
          src: vilhelm3b,
          caption: 'Katarina Ivanovic - Still life with goldfinch'
        }, {
          src: vilhelm3c,
          caption: 'Olaf Rude - Still life with tulips in a vase'
        }],
        answer: 'Still lifes has a long tradition in art, starting from 1600 Netherlands. They were originally called Menento Mori, which translates to ‘Remember you have to die’.  These first still life’s where about the fragility of life, and often carried hidden symbols. Lundstrøm was however more interested in the colors, compositions and shapes that could be represented through still lifes.',
        link: {
          to: '/explore',
          text: 'explore still lifes'
        }
      }, {
        question: 'Other pictures by Lundstrøm in the collection',
        image: [{
          src: vilhelm6a,
          caption: 'Mor og barn'
        }, {
          src: vilhelm6b,
          caption: 'Stående model'
        }, {
          src: vilhelm1,
          caption: 'Opstilling med Kander'
        }],
        answer: 'Tap below to find other works by Lundstrøm in the museum.',
        link: {
          to: '/map/lundstrom',
          text: 'find other works'
        }
      }]
    },

    {
      headline: 'Italian fieldworkers, Abruzzo',
      subheadline: 'P.S. Krøyer, oil on canvas, 1880',
      defaultImage: koyer1,
      color: '#829fa1',
      questions: [{
        question: 'Who was P.S. Krøyer ?',
        image: koyer,
        answer: 'Peder Severin Krøyer (1851-1909) is one of the most important figures in Danish art history. He is most often associated with the Danish groups of artist called Skagensmalerne, who painted at Skagen at the very northern part of Denmark.',
        link: {
          to: '/bio/koyer',
          text: 'full bio'
        }
      }, {
        question: 'Why is this painting important?',
        image: [{
          src: koyer1,
          caption: 'Italian fieldworkers'
        },{
          src: koyer3,
          caption: 'Italian Village Hatmakers'
        },{
          src: koyer4,
          caption: 'The garden at Albergo del Lily in a litter'
        }, {
          src: koyer2a,
          caption: 'A letter with sketches'
        }],
        answer: 'The painting was made during a trip to Italy, and show Krøyer as part of the Modern Breakthrough. The Modern Breakthrough was a period from approx 1870-1900 in Scandinavian art and literature, where  accurate portrayals of the world are pursued rather than romanticism.',
        link: {
          to: '/bio/letter',
          text: 'more info'
        }
      }, {
        question: 'The Modern Breakthrough and Skagensmalerne',
        image: [{
          src: koyer3,
          caption: 'P.S. Krøyer'
        },{
          src: koyer2b,
          caption: 'H. A. Brendekilde'
        },{
          src: koyer2c,
          caption: 'L. A. Ring'
        }],
        answer: 'Modern Breakthrough came to identify the artistic currents embedded in social realism and naturalism that grew in Scandinavia during the 1870s and the 1880s. Although closely related to European literary currents of the age, the Modern Breakthrough developed as a distinctly Scandinavian phenomenon.\n\nP S Krøyer is most famous as part of the artist group Skagensmalerne. The entire group was part of the Modern Breakthrough, and often depicted the lives and social realities of fishermen. For such depictions, the artists were often criticised, though it proved very influential at the time.',
        link: [{
          to: '/bio/skagensmalerne',
          text: 'MORE ON SKAGENSMALERNE'
        }]
      }, {
        question: 'related exhibition',
        image: [{
          src: koyer3,
          caption: 'P.S. Krøyer'
        },{
          src: koyer2b,
          caption: 'H. A. Brendekilde'
        },{
          src: koyer2c,
          caption: 'L. A. Ring'
        }],
        answer: 'Brandts will be holding an exhibition on the works of two prominent artists associated with the modern breakthrough - L.A. RING and H.A. Brendekilde - in early 2019.',
        link: [{
          to: '/exhibition',
          text: 'more info'
        }]
      }]
    }


  ]
  },
  mutations: {
    [SET_ARTIFACT_ID]: (state, id) => { state.id = id }
  }
}
