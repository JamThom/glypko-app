import img1 from  'Assets/photos/img1.jpg'
import img2 from  'Assets/photos/img2.jpg'
import img3 from  'Assets/photos/img3.jpg'
import img4 from  'Assets/photos/img4.jpg'
import img5 from  'Assets/photos/img5.jpg'
import img6 from  'Assets/photos/img6.jpg'
import img7 from  'Assets/photos/img7.jpg'
import img8 from  'Assets/photos/img8.jpg'
import img9 from  'Assets/photos/img9.jpg'
import img10 from  'Assets/photos/img10.jpg'

export default {
  state: {
    list: [{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img1
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img2
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img3
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img4
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img5
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img6
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img7
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img8
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img9
    },{
      date: Math.random(),
      headline: 'Example 1',
      subheadline: '1900 France',
      image: img10
    }]
  }
}
