
import koyer from 'Assets/photos/koyer.jpg'
import vilhelm6b from  'Assets/photos/vilhel.jpg'
import koyer2a from 'Assets/photos/koyer2a.png'
import koyer3 from 'Assets/photos/koyer3.png'
import koyer1 from 'Assets/photos/koyer11.jpg'
import koyer14 from 'Assets/photos/dep.jpg'

export default {
  state: {
    people: {
      'koyer': {
        images: [koyer],
        color: '#1071bf',
      },
      'letter': {
        images: [koyer2a,koyer3,koyer1],
        color: 'rgb(167, 155, 69)',
        sections: []
      },
      'lundstrom': {
        images: [vilhelm6b],
        color: '#8f6d9f'
      },
      'skagensmalerne': {
        images: [koyer14],
        color: '#8f6d9f'
      },
    }
  }
}
