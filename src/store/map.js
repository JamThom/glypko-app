import img1 from  'Assets/photos/img1.jpg'
import img2 from  'Assets/photos/img2.jpg'
import img3 from  'Assets/photos/koyer3.png'
import img4 from  'Assets/photos/img4.jpg'
import img5 from  'Assets/photos/img5.jpg'
import img6 from  'Assets/photos/img6.jpg'
import img7 from  'Assets/photos/img22.png'
import img8 from  'Assets/photos/vilhelm42.png'
import img9 from  'Assets/photos/mod.jpg'
import img10 from  'Assets/photos/nordicart_2.jpg'

export default {
  state: {
    headline: {
      all: 'you are here',
      lundstrom: 'related works',
      modern: 'modern breakthrough'
    },
    subheadline: {
      all: 'navigate your way around the museum',
      lundstrom: 'other works of Vilhelm Lundstrøm',
      modern: 'other works related to The modern breakthrough'
    },
    places: [{
      x: .793,
      y: .88,
      image: img9,
      headline: 'id sint qui',
      subheadline: 'dolor quis ullamco',
      description: 'Esse exercitation aliquip sit nostrud dolor quis tempor consectetur fugiat culpa cupidatat.',
      tags: ['all', 'modern']
    }, {
      x: .293,
      y: .88,
      image: img10,
      headline: 'id sint qui',
      subheadline: 'dolor quis ullamco',
      description: 'Esse exercitation aliquip sit nostrud dolor quis tempor consectetur fugiat culpa cupidatat.',
      tags: ['all', 'modern']
    }, {
      x: .893,
      y: .88,
      image: img1,
      headline: 'id sint qui',
      subheadline: 'dolor quis ullamco',
      description: 'Esse exercitation aliquip sit nostrud dolor quis tempor consectetur fugiat culpa cupidatat.',
      tags: ['all', 'modern']
    }, {
      x: .572,
      y: .88,
      image: img2,
      headline: 'id occaecat cillum',
      subheadline: 'in deserunt exercitation',
      description: 'Et deserunt nulla aliquip Duis veniam, Lorem dolor ex in aute irure.',
      tags: ['all', 'lundstrom']
    }, {
      x: .033,
      y: .64,
      image: img3,
      headline: 'mollit Lorem incididunt',
      subheadline: 'ut deserunt',
      description: 'Duis consectetur nisi Lorem ullamco amet, consequat. labore id ipsum aliquip sunt.',
      tags: ['all', 'modern']
    }, {
      x: .574,
      y: .64,
      image: img4,
      headline: 'ut ad consectetur',
      subheadline: 'velit irure',
      description: 'Esse deserunt occaecat consectetur nostrud elit, magna in pariatur. proident, id eiusmod.',
      tags: ['all']
    }, {
      x: .25,
      y: .50,
      image: img5,
      headline: 'est nostrud ipsum',
      subheadline: 'aute deserunt cupidatat',
      description: 'Dolor culpa elit, non nisi ex est reprehenderit ea proident, veniam, sit.',
      tags: ['all']
    }, {
      x: .628,
      y: .50,
      image: img6,
      headline: 'culpa voluptate pariatur',
      subheadline: 'nostrud tempor',
      description: 'ipsum cillum in aute non nostrud Excepteur adipisicing occaecat do tempor eiusmod.',
      tags: ['all']
    }, {
      x: .261,
      y: .88,
      image: img7,
      headline: 'est nostrud ipsum',
      subheadline: 'aute deserunt cupidatat',
      description: 'Dolor culpa elit, non nisi ex est reprehenderit ea proident, veniam, sit.',
      tags: ['all', 'lundstrom']
    }, {
      x: .328,
      y: .64,
      image: img8,
      headline: 'culpa voluptate pariatur',
      subheadline: 'nostrud tempor',
      description: 'ipsum cillum in aute non nostrud Excepteur adipisicing occaecat do tempor eiusmod.',
      tags: ['all', 'lundstrom']
    }]
  }
}
