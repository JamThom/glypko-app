import Vuex from 'vuex'
import Vue from 'vue'

import artifact from './artifact'
import popup from './popup'
import map from './map'
import tours from './tours'
import timeline from './timeline'
import bios from './bios'
import slides from './slides'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    artifact,
    popup,
    tours,
    timeline,
    map,
    bios,
    slides
  }
})

export { SET_ARTIFACT_ID, OPEN_COMING_SOON_POPUP, CLOSE_POPUP } from './constants'
