import { OPEN_COMING_SOON_POPUP, CLOSE_POPUP } from './constants'

export default {
  state: {
    visible: false,
    text: ''
  },
  mutations: {
    [OPEN_COMING_SOON_POPUP]: (state) => {
      state.visible = true
      state.text = 'Feature not built yet (WIP)'
    },
    [CLOSE_POPUP]: (state) => {
      state.visible = false
    }
  }
}
