import koyer1a from 'Assets/photos/koyer1a.png'
import koyer1b from 'Assets/photos/koyer1b.png'
import koyer1c from 'Assets/photos/koyer1c.png'
import koyer2 from 'Assets/photos/koyer2.png'

import k1 from 'Assets/photos/k1.jpg'
import k2 from 'Assets/photos/k2.jpg'
import a1 from 'Assets/photos/a1.jpg'
import a2 from 'Assets/photos/a2.jpg'

import vil1 from 'Assets/photos/vil1.jpg'
import vil2 from 'Assets/photos/vil2.jpg'

import shop1 from 'Assets/photos/shop1.png'
import shop2 from 'Assets/photos/shop2.png'

export default {
  state: {
    lists: {
      self: [
        { image: koyer2, text: 'Selfportrait, 1888', color: '#2586c3' },
        { image: koyer1a, text: 'Doubleportrait, 1890', color: '#4e6d86' },
        { image: koyer1c, text: 'Selfportrait, 1899', color: '#4c75c7' },
        { image: koyer1b, text: 'last selfportrait, 1909', color: '#efba1e' },
      ],
      compare: [
        { image: k1, text: 'Krøyer - At the victualler\'s when there is no fishing', color: '#2586c3' },
        { image: a1, text: 'Ancher - A winter day when there is no fishing', color: '#4c75c7' },
        { image: k2, text: 'Krøyer - Fishermen reeling in the net', color: '#4e6d86' },
        { image: a2, text: 'Ancher - Fishermen pulling in the net at Skagen', color: '#efba1e' },
      ],
      lundstrom: [
        { image: vil1, text: 'Lunch in the green, 1920', color: '#2586c3' },
        { image: vil2, text: 'The Four (Hommage aux peintres artistes), 1920', color: '#4e6d86' }
      ],
      prints: [
        { image: shop1, text: 'opstailling med kander', color: '#2586c3' },
        { image: shop2, text: 'Nature Morte. ca. 1923-25', color: '#4e6d86' }
      ]
    }
  }
}
