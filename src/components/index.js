const req = require.context("../components/global", true, /.+\.vue/)

req.keys().forEach(function(key){
  const component = require(`./global/${key.replace('./','')}`).default
  if (component.name) {
    module.exports[component.name] = component
  }
});
