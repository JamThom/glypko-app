function mounted() {

  const add = () => {
    document.addEventListener('touchmove', mm)
  }

  const mm = ({ touches }) => {
    const { offsetLeft, offsetWidth } = this.$el
    const ballWidth = this.$refs.ball.offsetWidth
    const v = (touches[0].clientX - offsetLeft - (ballWidth/2)) / (offsetWidth - ballWidth)
    this.$emit('update',Math.min(Math.max(v, 0), 1))
  }

  const remove = () => {
    document.removeEventListener('touchmove', mm)
  }

  this.$el.addEventListener('touchstart', add.bind(this))
  this.$el.addEventListener('touchend', remove.bind(this))

}

export default {
  mounted
}
