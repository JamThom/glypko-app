import * as d3 from 'd3'
import anime from 'animejs'
import getFormattedData from './puzzles-data'

export default class Pack {
  constructor ({ update, element, diameter }) {
    this.update = update
    this.div = d3.select(element)
    this.div.append('div')
    this.diameter = diameter
    this.pack = this.createPack()
    this.nodes = this.build(getFormattedData())
  }

  reload (d) {
    this.update(getFormattedData(d.data.name))
    const bubbles = this.div.selectAll('.child-bubble')
    anime.timeline()
      .add({
        targets: bubbles._groups,
        scale: [1, .9],
        opacity: [1, 0],
        duration: 300,
        delay: (x,i) => i * 20,
        easing: 'easeInOutQuad',
        complete: () => {
          bubbles.remove()
          this.build(getFormattedData(d.data.name))
        }
      })
  }

  createPack() {
    return d3.pack()
      .size([this.diameter/3, this.diameter/3])
  }

  createNodes(root) {
    let nodes = this.div.selectAll('.bubble')
      .data(this.pack(root).descendants())
      .enter().append('div')
      .attr('class', d => d.children ? 'bubble parent-bubble' : 'bubble child-bubble')
      .attr('style', this.generateBubbleStyle)
      .on('click', d => this.reload(d))
    this.animateIn(nodes._groups)
    return nodes
  }

  animateIn(nodes) {
  anime.timeline()
    .add({
      targets: nodes,
      opacity: [0, 1],
      duration: 300,
      delay: (x,i) => i * 20,
      easing: 'easeInOutQuad',
    })
  }

  generateBubbleStyle(d) {
    return `
      top: ${d.y - (d.r) + (d.children ? 0 : .5)}em;
      left: ${d.x - (d.r) + (d.children ? 0 : .5)}em;
      width: ${d.r*2 - (d.children ? 0 : 1)}em;
      height: ${d.r*2 - (d.children ? 0 : 1)}em;
      background-image: ${d.children ? 'none' : `url(${d.data.image})`};
    `
  }

  generateTextStyle(d) {
    const maxChars = d.data.name.split(' ').sort((a,b) => a.length > b.length ? -1 : 1)[0].length
    return `
      font-size: ${(d.r / 1.2) / Math.sqrt(maxChars)}em
    `
  }

  createTexts(nodes) {
    nodes.each(({data},i,node)=>{
      if (data.name.indexOf('Picture') === -1) {
        d3.select(node[i]).append('span')
          .attr('style', this.generateTextStyle)
          .text(({data}) => data.name)
        }
    })
  }

  createRoot(root) {
    return d3.hierarchy(root)
      .sum(d => d.size)
      .sort((a,b) => b.value - a.value)
  }

  destroyNodes() {
  }

  build(data) {
      const root = this.createRoot(data)
      const nodes = this.createNodes(root)
      this.createTexts(nodes)
      return nodes
  }

}
