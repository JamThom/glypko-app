import IMAGE_ARTISTS from 'Assets/photos/bubbles/artists.jpg'
import IMAGE_BREAKFAST from 'Assets/photos/bubbles/breakfast.jpg'
import IMAGE_CONTEMPORARY from 'Assets/photos/bubbles/contemporary.jpg'
import IMAGE_CROCKERY from 'Assets/photos/bubbles/crockery.jpg'
import IMAGE_CUBISM from 'Assets/photos/bubbles/cubism.jpg'
import IMAGE_DUTCH from 'Assets/photos/bubbles/dutch.jpg'
import IMAGE_EMIL_NOLDE from 'Assets/photos/bubbles/emil.jpg'
import IMAGE_EXPRESSIONISM from 'Assets/photos/bubbles/expressionism.jpg'
import IMAGE_FLOWERS from 'Assets/photos/bubbles/flowers.jpg'
import IMAGE_FOOD from 'Assets/photos/bubbles/food.jpg'
import IMAGE_IMPRESSIONISM from 'Assets/photos/bubbles/impressionism.jpg'
import IMAGE_MODERNISM from 'Assets/photos/bubbles/modernism.jpg'
import IMAGE_PABLO_PICASSO from 'Assets/photos/bubbles/picasso.jpg'
import IMAGE_PICTURE_1 from 'Assets/photos/bubbles/picture1.jpg'
import IMAGE_PICTURE_2 from 'Assets/photos/bubbles/picture2.jpg'
import IMAGE_PICTURE_3 from 'Assets/photos/bubbles/picture3.png'
import IMAGE_PICTURE_4 from 'Assets/photos/bubbles/picture4.jpg'
import IMAGE_PICTURE_5 from 'Assets/photos/bubbles/picture5.jpg'
import IMAGE_PICTURE_6 from 'Assets/photos/bubbles/picture6.jpg'
import IMAGE_PICTURE_7 from 'Assets/photos/bubbles/picture7.jpg'
import IMAGE_PICTURE_8 from 'Assets/photos/bubbles/picture8.jpg'
import IMAGE_PICTURE_9 from 'Assets/photos/bubbles/picture9.jpg'
import IMAGE_PICTURE_10 from 'Assets/photos/bubbles/picture10.jpg'
import IMAGE_PICTURE_11 from 'Assets/photos/bubbles/picture11.jpg'
import IMAGE_PICTURE_13 from 'Assets/photos/bubbles/picture13.jpg'
import IMAGE_PICTURE_12 from 'Assets/photos/bubbles/picture12.jpg'
import IMAGE_PICTURE_14 from 'Assets/photos/bubbles/picture14.jpg'
import IMAGE_PICTURE_15 from 'Assets/photos/bubbles/picture15.jpg'
import IMAGE_PICTURE_16 from 'Assets/photos/bubbles/picture16.jpg'
import IMAGE_PICTURE_17 from 'Assets/photos/bubbles/picture17.jpg'
import IMAGE_PICTURE_18 from 'Assets/photos/bubbles/picture18.jpg'
import IMAGE_PICTURE_19 from 'Assets/photos/bubbles/picture19.jpg'
import IMAGE_PICTURE_20 from 'Assets/photos/bubbles/picture20.jpg'
import IMAGE_PICTURE_21 from 'Assets/photos/bubbles/picture21.jpg'
import IMAGE_PICTURE_22 from 'Assets/photos/bubbles/picture22.jpg'
import IMAGE_PICTURE_23 from 'Assets/photos/bubbles/picture23.jpg'
import IMAGE_PICTURE_24 from 'Assets/photos/bubbles/picture24.jpg'
import IMAGE_PICTURE_25 from 'Assets/photos/bubbles/picture25.jpg'
import IMAGE_PICTURE_26 from 'Assets/photos/bubbles/picture26.jpg'
import IMAGE_PICTURE_27 from 'Assets/photos/bubbles/picture27.jpg'
import IMAGE_PICTURE_28 from 'Assets/photos/bubbles/picture28.jpg'
import IMAGE_PICTURE_29 from 'Assets/photos/bubbles/picture29.jpg'
import IMAGE_PICTURE_30 from 'Assets/photos/bubbles/picture30.jpg'
import IMAGE_PICTURE_31 from 'Assets/photos/bubbles/picture31.jpg'
import IMAGE_PICTURE_32 from 'Assets/photos/bubbles/picture32.jpg'
import IMAGE_PICTURE_33 from 'Assets/photos/bubbles/picture33.jpg'
import IMAGE_PICTURE_34 from 'Assets/photos/bubbles/picture34.jpg'
import IMAGE_PICTURE_35 from 'Assets/photos/bubbles/picture35.jpg'
import IMAGE_PIETER_CLAESZ from 'Assets/photos/bubbles/pieterclaesz.jpg'
import IMAGE_HOOGSTRATEN from 'Assets/photos/bubbles/samuel.jpg'
import IMAGE_ORLA_MUFF from 'Assets/photos/bubbles/orlamuff.jpg'
import IMAGE_SEA_FOOD from 'Assets/photos/bubbles/seafood.jpg'
import IMAGE_STILL_LIFES from 'Assets/photos/bubbles/stilllives.jpg'
import IMAGE_SURREALISM from 'Assets/photos/bubbles/surrealism.jpg'
import IMAGE_TROMPE_LOIL from 'Assets/photos/bubbles/trompelil.jpg'
import IMAGE_VAN_GOGH from 'Assets/photos/bubbles/vangogh.jpg'
import IMAGE_VANITAS from 'Assets/photos/bubbles/vanitas.jpg'
import IMAGE_VILHELM_LUNDSTROM from 'Assets/photos/bubbles/vilhelm.jpg'
import IMAGE_PAUL_CEZANNE from 'Assets/photos/bubbles/paul-cezanne.jpg'
import IMAGE_ABSTRACT from 'Assets/photos/bubbles/abstract.jpg'
import IMAGE_SALVADOR_DALI from 'Assets/photos/bubbles/dali.jpg'

const STILL_LIFES = 'still lifes'
const MODERNISM = 'modernism'
const DUTCH = 'Dutch'
const DANISH = 'Danish'
const TROMPE_LOIL = 'Trompe-l\'œil'
const BREAKFAST = 'Breakfast'
const SURREALISM = 'Surrealism'
const VANITAS = 'Vanitas'
const PABLO_PICASSO = 'Pablo Picasso'
const SALVADOR_DALI = 'Salvador Dali'
const VAN_GOGH = 'Van Gogh'
const PIETER_CLAESZ = 'Pieter Claesz'
const HOOGSTRATEN = 'Samuel Dirkszoon van Hoogstraten'
const EMIL_NOLDE = 'Emil Nolde'
const ARTISTS = 'Artists'
const ABSTRACT = 'Abstract'
const EXPRESSIONISM = 'Expressionism'
const IMPRESSIONISM = 'Impressionism'
const FLOWERS = 'Flowers'
const FOOD = 'Food'
const CROCKERY = 'Crockery'
const VILHELM_LUNDSTROM = 'Vilhelm LundstrOm'
const CUBISM = 'Cubism'
const SEA_FOOD = 'Sea-food'
const CONTEMPORARY = 'Contemporary'
const ORLA_MUFF = 'Orla Muff'
const PAUL_CEZANNE = 'Paul Cezanne'
const PICTURE_1 = 'Picture_1'
const PICTURE_2 = 'Picture 2'
const PICTURE_3 = 'Picture 3'
const PICTURE_4 = 'Picture 4'
const PICTURE_5 = 'Picture 5'
const PICTURE_6 = 'Picture 6'
const PICTURE_7 = 'Picture 7'
const PICTURE_8 = 'Picture 8'
const PICTURE_9 = 'Picture 9'
const PICTURE_10 = 'Picture 10'
const PICTURE_11 = 'Picture 11'
const PICTURE_12 = 'Picture 12'
const PICTURE_13 = 'Picture 13'
const PICTURE_14 = 'Picture 14'
const PICTURE_15 = 'Picture 15'
const PICTURE_16 = 'Picture 16'
const PICTURE_17 = 'Picture 17'
const PICTURE_18 = 'Picture 18'
const PICTURE_19 = 'Picture 19'
const PICTURE_20 = 'Picture 20'
const PICTURE_21 = 'Picture 21'
const PICTURE_22 = 'Picture 22'
const PICTURE_23 = 'Picture 23'
const PICTURE_24 = 'Picture 24'
const PICTURE_25 = 'Picture 25'
const PICTURE_26 = 'Picture 26'
const PICTURE_27 = 'Picture 27'
const PICTURE_28 = 'Picture 28'
const PICTURE_29 = 'Picture 29'
const PICTURE_30 = 'Picture 30'
const PICTURE_31 = 'Picture 31'
const PICTURE_32 = 'Picture 32'
const PICTURE_33 = 'Picture 33'
const PICTURE_34 = 'Picture 34'
const PICTURE_35 = 'Picture 35'



const items = {
  [STILL_LIFES]: {
    name: STILL_LIFES,
    description: '',
    image: IMAGE_STILL_LIFES,
    tags: [{name:MODERNISM,size:3.141}, {name:DANISH,size:3.091}, {name:TROMPE_LOIL,size:2.090}, {name:ARTISTS,size:4.179}, {name:FLOWERS,size:1.039}, {name:FOOD,size:1.195}]
  },
  [MODERNISM]: {
    name: MODERNISM,
    description: '',
    image: IMAGE_MODERNISM,
    tags: [{name: SURREALISM, size:1.009}, {name: PABLO_PICASSO, size:2.195}, {name: EMIL_NOLDE, size:1.3}, {name: EXPRESSIONISM, size:1.145}, {name: CUBISM, size:2.051},{name:VILHELM_LUNDSTROM,size:3.030}]
  },
  [DUTCH]: {
    name: DUTCH,
    description: '',
    image: IMAGE_DUTCH,
    tags: [{name:HOOGSTRATEN,size:2.106}, {name: VAN_GOGH, size:3.183}, {name:PIETER_CLAESZ,size:2.046}, {name:VANITAS,size:1.190}]
  },
  [DANISH]: {
    name: DANISH,
    description: '',
    image: IMAGE_DUTCH,
    tags: [{name:VILHELM_LUNDSTROM,size:3.052}, {name:EMIL_NOLDE,size:2.105}, {name:ORLA_MUFF,size:1.018}]
  },
  [TROMPE_LOIL]: {
    name: TROMPE_LOIL,
    description: '',
    image: IMAGE_TROMPE_LOIL,
    tags: [{name:PICTURE_30,size:1.003}, {name:PICTURE_31,size:1.016}, {name:PICTURE_32,size:1.112}, {name:PICTURE_33,size:1.144}, {name:PICTURE_34,size:1.128}, {name:PICTURE_35,size:1.036}, {name:DUTCH,size:1.142}]
  },
  [BREAKFAST]: {
    name: BREAKFAST,
    description: '',
    image: IMAGE_BREAKFAST,
    tags: [{name:FOOD,size:1.092}, {name:PICTURE_1,size:1.073}, {name:PICTURE_2,size:1.061}, {name:PICTURE_3,size:1.137}, {name:PICTURE_4,size:1.151}, {name:PICTURE_5,size:1.017}, {name:PICTURE_28,size:1.063}]
  },
  [SURREALISM]: {
    name: SURREALISM,
    description: '',
    image: IMAGE_SURREALISM,
    tags: [{name:PICTURE_13,size:2.080}, {name:PICTURE_14,size:2.116}, {name:PABLO_PICASSO,size:1.071}, {name:SALVADOR_DALI,size:1.015}, {name:STILL_LIFES,size:1.138}, {name:MODERNISM,size:1.147}]
  },
  [VANITAS]: {
    name: VANITAS,
    description: '',
    image: IMAGE_VANITAS,
    tags: [{name:PICTURE_9,size:2.152}, {name:PICTURE_10,size:3.106}, {name:PICTURE_11,size:3.136}, {name:PICTURE_12,size:3.018}, {name:PICTURE_13,size:1.170}, {name:PICTURE_15,size:1.128}, {name:DUTCH,size:1.084}]
  },
  [PABLO_PICASSO]: {
    name: PABLO_PICASSO,
    description: '',
    image: IMAGE_PABLO_PICASSO,
    tags: [{name:PICTURE_14,size:3.052}, {name:PICTURE_15,size:3.182}, {name:PICTURE_16,size:3.120}, {name:SURREALISM,size:2.125}, {name:ABSTRACT,size:2.059}, {name:MODERNISM,size:2.071}]
  },
  [VAN_GOGH]: {
    name: VAN_GOGH,
    description: '',
    image: IMAGE_VAN_GOGH,
    tags: [{name:ARTISTS,size:1.108}, {name:PICTURE_27,size:3.077}, {name:PICTURE_28,size:3.173}, {name:IMPRESSIONISM,size:2.153}, {name:MODERNISM,size:2.083}]
  },
  [PIETER_CLAESZ]: {
    name: PIETER_CLAESZ,
    description: '',
    image: IMAGE_PIETER_CLAESZ,
    tags: [{name:PICTURE_6,size:3.171}, {name:PICTURE_9,size:3.016}, {name:DUTCH,size:2.065}]
  },
  [HOOGSTRATEN]: {
    name: HOOGSTRATEN,
    description: '',
    image: IMAGE_HOOGSTRATEN,
    tags: [{name:PICTURE_30,size:3.082}, {name:PICTURE_32,size:3.068}, {name:DUTCH,size:2.136}]
  },
  [EMIL_NOLDE]: {
    name: EMIL_NOLDE,
    description: '',
    image: IMAGE_EMIL_NOLDE,
    tags: [{name:MODERNISM,size:1.132}, {name:PICTURE_17,size:3.135}, {name:PICTURE_18,size:3.077}, {name:DANISH,size:2.019}]
  },
  [ARTISTS]: {
    name: ARTISTS,
    description: '',
    image: IMAGE_ARTISTS,
    tags: [{name:PABLO_PICASSO,size:3.024},{name:VILHELM_LUNDSTROM,size:4.024},{name:SALVADOR_DALI,size:1.151},{name:VAN_GOGH,size:3.098},{name:PIETER_CLAESZ,size:2.032},{name:HOOGSTRATEN,size:3.149},{name:EMIL_NOLDE,size:2.004},{name:ORLA_MUFF,size:1.027},{name:PAUL_CEZANNE,size:1.025}]
  },
  [EXPRESSIONISM]: {
    name: EXPRESSIONISM,
    description: '',
    image: IMAGE_EXPRESSIONISM,
    tags: [{name:PICTURE_13,size:1.009}, {name:PICTURE_17,size:1.174}, {name:PICTURE_18,size:1.020}, {name:EMIL_NOLDE,size:1.104}, {name:SALVADOR_DALI,size:1.142}, {name:MODERNISM,size:1.182}]
  },
  [IMPRESSIONISM]: {
    name: IMPRESSIONISM,
    description: '',
    image: IMAGE_IMPRESSIONISM,
    tags: [{name:MODERNISM,size:1.178}, {name:PICTURE_25,size:1.009}, {name:PICTURE_26,size:1.000}, {name:PICTURE_27,size:1.083}, {name:PICTURE_28,size:1.101}, {name:PAUL_CEZANNE,size:1.099}, {name:VAN_GOGH,size:1.013}]
  },
  [FLOWERS]: {
    name: FLOWERS,
    description: '',
    image: IMAGE_FLOWERS,
    tags: [{name:PICTURE_17,size:1.185}, {name:PICTURE_18,size:1.122}, {name:STILL_LIFES,size:1.071}]
  },
  [FOOD]: {
    name: FOOD,
    description: '',
    image: IMAGE_FOOD,
    tags: [{name:PICTURE_1,size:1.053},{name:PICTURE_2,size:2.094},{name:PICTURE_3,size:3.088},{name:PICTURE_4,size:4.129},{name:PICTURE_5,size:1.178},{name:PICTURE_6,size:2.174},{name:PICTURE_7,size:3.032},{name:PICTURE_8,size:4.139}, {name:PICTURE_15,size:1.161}, {name:PICTURE_16,size:2.081}, {name:PICTURE_19,size:3.140}, {name:PICTURE_21,size:4.085}, {name:PICTURE_22,size:1.036}, {name:PICTURE_24,size:1.076}, {name:PICTURE_28,size:1.199}, {name:PICTURE_29,size:1.191}, {name:PICTURE_35,size:4.105}, {name:BREAKFAST,size:4.042}, {name:STILL_LIFES,size:4.096}]
  },
  [CROCKERY]: {
    name: CROCKERY,
    description: '',
    image: IMAGE_CROCKERY,
    tags: [{name:PICTURE_14,size:1.041}, {name:PICTURE_15,size:1.094}, {name:PICTURE_19,size:1.137}, {name:PICTURE_20,size:1.005}, {name:PICTURE_21,size:1.061}, {name:PICTURE_22,size:1.038}, {name:PICTURE_23,size:1.094}, {name:PICTURE_24,size:1.071}, {name:PICTURE_25,size:1.040}]
  },
  [VILHELM_LUNDSTROM]: {
    name: VILHELM_LUNDSTROM,
    description: '',
    image: IMAGE_VILHELM_LUNDSTROM,
    tags: [{name:DANISH,size:1.086}, {name:PICTURE_20,size:4.076}, {name:PICTURE_21,size:3.046}, {name:PICTURE_22,size:2.122}, {name:PICTURE_23,size:3.139}, {name:PICTURE_24,size:4.087}, {name:ARTISTS,size:1.167}]
  },
  [ORLA_MUFF]: {
    name: ORLA_MUFF,
    description: '',
    image: IMAGE_ORLA_MUFF,
    tags: [{name:DANISH,size:1.031}, {name:PICTURE_19,size:2.173}, {name:ARTISTS,size:1.167}]
  },
  [PAUL_CEZANNE]: {
    name: PAUL_CEZANNE,
    description: '',
    image: IMAGE_PAUL_CEZANNE,
    tags: [{name:IMPRESSIONISM,size:1.081}, {name:STILL_LIFES,size:1.070}, {name:PICTURE_25,size:3.086}, {name:ARTISTS,size:1.167}]
  },
  [CUBISM]: {
    name: CUBISM,
    description: '',
    image: IMAGE_CUBISM,
    tags: [{name:MODERNISM,size:1.049}, {name:PICTURE_16,size:3.199}, {name:PICTURE_20,size:1.067},  {name:PICTURE_21,size:1.004}, {name:PICTURE_22,size:1.092}, {name:PICTURE_23,size:1.170}, {name:PICTURE_24,size:1.094}, {name:PICTURE_35,size:1.045}]
  },
  [SEA_FOOD]: {
    name: SEA_FOOD,
    description: '',
    image: IMAGE_SEA_FOOD,
    tags: [{name:FOOD,size:1.034}]
  },
  [CONTEMPORARY]: {
    name: CONTEMPORARY,
    description: '',
    image: IMAGE_CONTEMPORARY,
    tags: [{name:PICTURE_3,size:1.161}, {name:PICTURE_4,size:1.105}, {name:PICTURE_7,size:1.029}, {name:PICTURE_34,size:1.179}]
  },
  [ABSTRACT]: {
    name: ABSTRACT,
    description: '',
    image: IMAGE_ABSTRACT,
    tags: [{name:PABLO_PICASSO, size:1.009},{name:PICTURE_15, size:1.158},{name:PICTURE_29, size:1.143}]
  },
  [SALVADOR_DALI]: {
    name: SALVADOR_DALI,
    description: '',
    image: IMAGE_SALVADOR_DALI,
    tags: [{name:SURREALISM,size:2.010}, {name:EXPRESSIONISM,size:1.167}, {name:ARTISTS,size:1.167}]
  },
  [PICTURE_1]: {
    name: PICTURE_1,
    description: '',
    image: IMAGE_PICTURE_1,
    tags: [{name:MODERNISM,size:2.012}, {name:STILL_LIFES,size:1.090}, {name:BREAKFAST,size:1.194}, {name:FOOD,size:1.159}, {name:ARTISTS,size:1.116}]
  },
  [PICTURE_2]: {
    name: PICTURE_2,
    description: '',
    image: IMAGE_PICTURE_2,
    tags: [{name:MODERNISM,size:2.068}, {name:STILL_LIFES,size:1.030}, {name:BREAKFAST,size:1.158}, {name:FOOD,size:1.127}, {name:ARTISTS,size:1.016}]
  },
  [PICTURE_3]: {
    name: PICTURE_3,
    description: '',
    image: IMAGE_PICTURE_3,
    tags: [{name:STILL_LIFES,size:1.085}, {name:BREAKFAST,size:1.000}, {name:FOOD,size:1.067}, {name:CONTEMPORARY,size:2.113}, {name:ARTISTS,size:1.054}]
  },
  [PICTURE_4]: {
    name: PICTURE_4,
    description: '',
    image: IMAGE_PICTURE_4,
    tags: [{name:STILL_LIFES,size:1.037}, {name:BREAKFAST,size:1.047}, {name:FOOD,size:1.189}, {name:CONTEMPORARY,size:2.062}, {name:ARTISTS,size:1.035}]
  },
  [PICTURE_5]: {
    name: PICTURE_5,
    description: '',
    image: IMAGE_PICTURE_5,
    tags: [{name:STILL_LIFES,size:1.042}, {name:BREAKFAST,size:1.128}, {name:FOOD,size:1.166}, {name:DUTCH,size:2.020}, {name:ARTISTS,size:1.069}]
  },
  [PICTURE_6]: {
    name: PICTURE_6,
    description: '',
    image: IMAGE_PICTURE_6,
    tags: [{name:STILL_LIFES,size:1.025}, {name:FOOD,size:1.084}, {name:DUTCH,size:2.189}, {name:ARTISTS,size:1.191}, {name:PIETER_CLAESZ,size:3.074}, {name:SEA_FOOD,size:1.068}]
  },
  [PICTURE_7]: {
    name: PICTURE_7,
    description: '',
    image: IMAGE_PICTURE_7,
    tags: [{name:STILL_LIFES,size:1.090}, {name:FOOD,size:1.064}, {name:CONTEMPORARY,size:2.003}, {name:ARTISTS,size:1.010}]
  },
  [PICTURE_8]: {
    name: PICTURE_8,
    description: '',
    image: IMAGE_PICTURE_8,
    tags: [{name:STILL_LIFES,size:1.063}, {name:FOOD,size:1.017}, {name:DUTCH,size:2.172}, {name:ARTISTS,size:1.165}]
  },
  [PICTURE_9]: {
    name: PICTURE_9,
    description: '',
    image: IMAGE_PICTURE_9,
    tags: [{name:STILL_LIFES,size:1.003}, {name:DUTCH,size:2.120}, {name:VANITAS,size:2.161}, {name:ARTISTS,size:1.044}, {name:PIETER_CLAESZ,size:3.007}]
  },
  [PICTURE_10]: {
    name: PICTURE_10,
    description: '',
    image: IMAGE_PICTURE_10,
    tags: [{name:STILL_LIFES,size:1.192}, {name:DUTCH,size:2.057}, {name:VANITAS,size:2.044}]
  },
  [PICTURE_11]: {
    name: PICTURE_11,
    description: '',
    image: IMAGE_PICTURE_11,
    tags: [{name:STILL_LIFES,size:1.042}, {name:DUTCH,size:2.113}, {name:VANITAS,size:2.181}]
  },
  [PICTURE_12]: {
    name: PICTURE_12,
    description: '',
    image: IMAGE_PICTURE_12,
    tags: [{name:STILL_LIFES,size:1.059}, {name:DUTCH,size:2.034}, {name:VANITAS,size:2.186}]
  },
  [PICTURE_13]: {
    name: PICTURE_13,
    description: '',
    image: IMAGE_PICTURE_13,
    tags: [{name:STILL_LIFES,size:1.079}, {name:MODERNISM,size:2.144}, {name:SURREALISM,size:3.088}, {name:VANITAS,size:1.053}, {name:EXPRESSIONISM,size:1.164}]
  },
  [PICTURE_14]: {
    name: PICTURE_14,
    description: '',
    image: IMAGE_PICTURE_14,
    tags: [{name:STILL_LIFES,size:1.109}, {name:MODERNISM,size:2.115}, {name:CROCKERY,size:1.114}, {name:SURREALISM,size:3.100}, {name:PABLO_PICASSO,size:4.153}]
  },
  [PICTURE_15]: {
    name: PICTURE_15,
    description: '',
    image: IMAGE_PICTURE_15,
    tags: [{name:STILL_LIFES,size:1.024}, {name:MODERNISM,size:3.178}, {name:VANITAS,size:1.062}, {name:ABSTRACT,size:3.107}, {name:CROCKERY,size:1.164}, {name:FOOD,size:1.064}, {name:PABLO_PICASSO,size:4.140}]
  },
  [PICTURE_16]: {
    name: PICTURE_16,
    description: '',
    image: IMAGE_PICTURE_16,
    tags: [{name:STILL_LIFES,size:1.146}, {name:MODERNISM,size:3.068}, {name:ABSTRACT,size:2.182}, {name:FOOD,size:1.034}, {name:PABLO_PICASSO,size:3.010}, {name:CUBISM,size:4.148}]
  },
  [PICTURE_17]: {
    name: PICTURE_17,
    description: '',
    image: IMAGE_PICTURE_17,
    tags: [{name:STILL_LIFES,size:1.197}, {name:EXPRESSIONISM,size:1.045}, {name:MODERNISM,size:2.190}, {name:FLOWERS,size:1.097}, {name:DANISH,size:3.076}, {name:EMIL_NOLDE,size:4.014}]
  },
  [PICTURE_18]: {
    name: PICTURE_18,
    description: '',
    image: IMAGE_PICTURE_18,
    tags: [{name:STILL_LIFES,size:1.086}, {name:EXPRESSIONISM,size:1.151}, {name:MODERNISM,size:2.144}, {name:FLOWERS,size:1.143}, {name:DANISH,size:3.021}, {name:EMIL_NOLDE,size:4.122}]
  },
  [PICTURE_19]: {
    name: PICTURE_19,
    description: '',
    image: IMAGE_PICTURE_19,
    tags: [{name:STILL_LIFES,size:1.163}, {name:MODERNISM,size:2.130}, {name:CROCKERY,size:1.052}, {name:FOOD,size:1.125}, {name:DANISH,size:1.176}]
  },
  [PICTURE_20]: {
    name: PICTURE_20,
    description: '',
    image: IMAGE_PICTURE_20,
    tags: [{name:MODERNISM,size:2.081}, {name:VILHELM_LUNDSTROM,size:3.122}, {name:STILL_LIFES,size:1.143}, {name:CUBISM,size:.89}, {name:CROCKERY,size:1.040}, {name:DANISH,size:2.090}]
  },
  [PICTURE_21]: {
    name: PICTURE_21,
    description: '',
    image: IMAGE_PICTURE_21,
    tags: [{name:MODERNISM,size:2.115}, {name:VILHELM_LUNDSTROM,size:3.000}, {name:STILL_LIFES,size:1.175}, {name:CUBISM,size:.88}, {name:CROCKERY,size:1.054}, {name:FOOD,size:1.038}, {name:DANISH,size:2.136}]
  },
  [PICTURE_22]: {
    name: PICTURE_22,
    description: '',
    image: IMAGE_PICTURE_22,
    tags: [{name:MODERNISM,size:2.067}, {name:VILHELM_LUNDSTROM,size:3.190}, {name:STILL_LIFES,size:1.038}, {name:CUBISM,size:.83}, {name:CROCKERY,size:1.172}, {name:FOOD,size:1.035}, {name:DANISH,size:2.034}]
  },
  [PICTURE_23]: {
    name: PICTURE_23,
    description: '',
    image: IMAGE_PICTURE_23,
    tags: [{name:MODERNISM,size:2.030}, {name:VILHELM_LUNDSTROM,size:3.063}, {name:STILL_LIFES,size:1.081}, {name:CUBISM,size:.89}, {name:CROCKERY,size:1.088}, {name:DANISH,size:2.156}]
  },
  [PICTURE_24]: {
    name: PICTURE_24,
    description: '',
    image: IMAGE_PICTURE_24,
    tags: [{name:MODERNISM,size:2.136}, {name:VILHELM_LUNDSTROM,size:3.106}, {name:STILL_LIFES,size:1.089}, {name:CUBISM,size:.81}, {name:CROCKERY,size:1.020}, {name:FOOD,size:1.072}, {name:DANISH,size:2.055}]
  },
  [PICTURE_25]: {
    name: PICTURE_25,
    description: '',
    image: IMAGE_PICTURE_25,
    tags: [{name:MODERNISM,size:1.147}, {name:IMPRESSIONISM,size:2.069}, {name:STILL_LIFES,size:1.058}, {name:FOOD,size:3.105}, {name:CROCKERY,size:1.041}, {name:PAUL_CEZANNE,size:4.052}]
  },
  [PICTURE_26]: {
    name: PICTURE_26,
    description: '',
    image: IMAGE_PICTURE_26,
    tags: [{name:MODERNISM,size:1.117}, {name:IMPRESSIONISM,size:2.082}, {name:STILL_LIFES,size:1.128}, {name:FLOWERS,size:4.122}]
  },
  [PICTURE_27]: {
    name: PICTURE_27,
    description: '',
    image: IMAGE_PICTURE_27,
    tags: [{name:IMPRESSIONISM,size:1.128}, {name:STILL_LIFES,size:2.121}, {name:FLOWERS,size:3.023}, {name:VAN_GOGH,size:4.037}]
  },
  [PICTURE_28]: {
    name: PICTURE_28,
    description: '',
    image: IMAGE_PICTURE_28,
    tags: [{name:MODERNISM,size:1.049}, {name:STILL_LIFES,size:1.132}, {name:BREAKFAST,size:2.134}, {name:FOOD,size:3.024}, {name:VAN_GOGH,size:4.177}, {name:IMPRESSIONISM,size:1.121}]
  },
  [PICTURE_29]: {
    name: PICTURE_29,
    description: '',
    image: IMAGE_PICTURE_29,
    tags: [{name:STILL_LIFES,size:1.097}, {name:MODERNISM,size:1.184}, {name:FOOD,size:3.137}, {name:ABSTRACT,size:1.176}]
  },
  [PICTURE_30]: {
    name: PICTURE_30,
    description: '',
    image: IMAGE_PICTURE_30,
    tags: [{name:STILL_LIFES,size:1.137}, {name:DUTCH,size:2.128}, {name:TROMPE_LOIL,size:2.000}, {name:HOOGSTRATEN,size:4.089}]
  },
  [PICTURE_31]: {
    name: PICTURE_31,
    description: '',
    image: IMAGE_PICTURE_31,
    tags: [{name:STILL_LIFES,size:1.047}, {name:DUTCH,size:3.101}, {name:TROMPE_LOIL,size:2.026}]
  },
  [PICTURE_32]: {
    name: PICTURE_32,
    description: '',
    image: IMAGE_PICTURE_32,
    tags: [{name:STILL_LIFES,size:1.008}, {name:DUTCH,size:1.067}, {name:TROMPE_LOIL,size:1.123}, {name:HOOGSTRATEN,size:1.185}]
  },
  [PICTURE_33]: {
    name: PICTURE_33,
    description: '',
    image: IMAGE_PICTURE_33,
    tags: [{name:STILL_LIFES,size:1.144}, {name:TROMPE_LOIL,size:2.181}]
  },
  [PICTURE_34]: {
    name: PICTURE_34,
    description: '',
    image: IMAGE_PICTURE_34,
    tags: [{name:STILL_LIFES,size:1.072}, {name:TROMPE_LOIL,size:1.059}, {name:CONTEMPORARY,size:1.083}]
  },
  [PICTURE_35]: {
    name: PICTURE_35,
    description: '',
    image: IMAGE_PICTURE_35,
    tags: [{name:STILL_LIFES,size:1.058}, {name:MODERNISM,size:1.189}, {name:FOOD,size:1.092}, {name:SEA_FOOD,size:1.034}, {name:CUBISM,size:1.182}]
  }
}



export default function getFormattedData(name) {
  name = name || STILL_LIFES
  return {
    name: items[name].name,
    image: items[name].image,
    description: items[name].description,
    children: items[name].tags.map(item => ({
      name: items[item.name].name,
      size: item.size,
      image: items[item.name].image
    }))
  }
}
