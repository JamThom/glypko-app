import Welcome from 'Routes/welcome'
import Bio from 'Routes/bio'
import Color from 'Routes/color'
import Exhibition from 'Routes/exhibition'
import Tour from 'Routes/tour'
import Collection from 'Routes/collection'
import Intro from 'Routes/intro'
import Matched from 'Routes/matched'
import On from 'Routes/on'
import Artifact from 'Routes/artifact'
import About from 'Routes/about'
import Help from 'Routes/help'
import Running from 'Routes/running'
import Solution from 'Routes/solution'
import Slider from 'Routes/slider'
import Explore from 'Routes/explore'
import Video from 'Routes/video'
import Map from 'Routes/map'
import Timeline from 'Routes/timeline'
import Skagensmalerne from 'Components/routes/bio/skagensmalerne'
import Koyer from 'Components/routes/bio/koyer'
import Lundstrom from 'Components/routes/bio/lundstrom'
import Letter from 'Components/routes/bio/letter'

import store from 'Store'

export default {
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      component: Welcome
    }, {
      path: '/intro',
      component: Intro
    }, {
      path: '/on',
      component: On,
      children: [{
        path: '/',
        component: Running,
        children: [
          {
            path: 'match/:id',
            component: Matched,
          }
        ]
      }]
    }, {
      path: '/artifact/:id',
      component: Artifact,
    }, {
      path: '/bio',
      component: Bio,
      children: [{
        path: 'skagensmalerne',
        component: Skagensmalerne,
      },{
        path: 'koyer',
        component: Koyer,
      },{
        path: 'lundstrom',
        component: Lundstrom,
      },{
        path: 'letter',
        component: Letter,
      }]
    }, {
      path: '/solution/:id',
      component: Solution,
    }, {
      path: '/slider/:id',
      component: Slider
    }, {
      path: '/map/:tag',
      component: Map
    }, {
      path: '/color',
      component: Color,
    }, {
      path: '/video/:id',
      component: Video,
    }, {
      path: '/timeline',
      component: Timeline,
    }, {
      path: '/exhibition',
      component: Exhibition,
    }, {
      path: '/tour',
      component: Tour,
    }, {
      path: '/explore',
      component: Explore
    }, {
      path: '/about',
      component: About
    }, {
      path: '/help',
      component: Help
    }, {
      path: '/',
      redirect: '/running'
    }, {
      path: '/collection',
      component: Collection
    }
  ]
}
