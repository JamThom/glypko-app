import thumb from './images/thumb.jpg'
import slide01 from './images/slide_01.jpg'
import slide02 from './images/slide_02.jpg'
import slide03 from './images/slide_03.jpg'
import slide04 from './images/slide_04.jpg'

export default {
  images: {
    thumb
  },
  messages: {
    en: {
      name: 'Burr puzzle knot',
      intro: '1928, China',
      subtitle: 'some subtitle text',
      description: 'A burr puzzle is an interlocking puzzle consisting of notched sticks, combined to make one three-dimensional, usually symmetrical unit. These puzzles are traditionally made of wood, but versions made of plastic or metal can also be found.'
    }
  },
  slides: {
    en: [{
      image: slide01,
      caption: 'In order to solve the Burr puzzle, you must first dismantle it completely and place all of the pieces in a chaotic pile.'
    }, {
      image: slide02,
      caption: 'Rearrange the pieces of the pile so that they look a little bit like a train track.'
    }, {
      image: slide03,
      caption: 'Now comes the fun part: jam the pieces together in as many ways as you can, trying out every possible compination or technique you can think of.'
    }, {
      image: slide04,
      caption: 'You have now completed your Burr puzzle knot.\nGood work!'
    }]
  },
  accordionItems: {
    en: [{
      question: 'What\'s a Burr Puzzle?',
      answer: 'This wood crafted, precision-made puzzle of intersecting cuboids supposedly resembles a burr seed (that famous inspiration for Velcro) upon completion. Not only is the name interesting, but the puzzle itself has been an inspiration to recreational mathematicians worldwide – just ask Bill Cutler, author and puzzle enthusiasts whose seminal works, Holey 6-Piece Burr! and A Computer Analysis of All 6-Piece Burrs have captured the hearts and minds of metagrobologists* everywhere. In this classic six-piece Burr, three sets of rod pairs need to intersect with each other to properly to solve the puzzle. (*metagrobology is the study of puzzles. If you get stuck with this puzzle, Bill Cutler lives in Illinois, USA. Just ask around once you get there, he is fairly well known and surely he’d be happy to help!) Dimensions: 2 1/3" x 2 1/3" x 2 1/3"'
    }, {
      question: 'History',
      answer: 'The term "burr" is first mentioned in a 1928 book by Edwin Wyatt, but the text implies that it was commonly used before. The term is attributed to the finished shape of many of these puzzles, resembling a seed burr. The origin of burr puzzles is unknown. The first known record[2] appears in a 1698 engraving used as a title page of Chambers\'s Cyclopaedia. Later records can be found in German catalogs from the late 18th century and early 19th century. There are claims of the burr being a Chinese invention, like other classic puzzles such as the Tangram. In Kerala these wooden problems are called edaakoodam.'
    }, {
      question: 'Structure',
      answer: 'All six pieces of the puzzle are square sticks of equal length (at least 3 times their width). When solved, the pieces are arranged in three perpendicular, mutually intersecting pairs. The notches of all sticks are located within the region of intersection, so when the puzzle is assembled they are unseen. All notches can be described as being made by removing cubic units (with an edge length of half the sticks\' width)'
    }]
  }
}
